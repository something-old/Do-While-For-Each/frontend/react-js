import React from "react";
import './Main.less';
import {Route, Router, Switch, withRouter} from "react-router-dom";
import {Login} from "../../Login/Login";

const MainComp = (props: any) => (
  <Router {...props}>

    <main className="main">
      <div className="app__adaptive-container">
        <div className="main__content">

          <Switch>

            <Route path="/" exact>
              <>
                <h2>Welcome!</h2>
                <p>Do|While|For|Each is a composite application.</p>
                <p><a href="https://gitlab.com/Do-While-For-Each/backend" target="_blank" rel="noopener noreferrer">Backend</a> is written in Java using microservice architecture.</p>
                <p>As a frontend you can use anything that can communicate with the backend API.</p>
                <p>While there is only two <a href="https://gitlab.com/Do-While-For-Each/frontend" target="_blank" rel="noopener noreferrer">frontend implementation</a>. You are now interacting with React implementation.</p>
                <p>--</p>
                <p>src: <a href="https://gitlab.com/Do-While-For-Each" target="_blank" rel="noopener noreferrer">gitlab.com/Do-While-For-Each</a></p>
              </>
            </Route>

            <Route path="/login" exact>
              <Login/>
            </Route>

          </Switch>

        </div>
      </div>
    </main>

  </Router>
);

export const Main = withRouter(MainComp);
