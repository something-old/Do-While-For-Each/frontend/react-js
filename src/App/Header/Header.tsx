import './Header.less';
import React from "react";
import {Button} from "@blueprintjs/core";
import {Link, Router, withRouter} from 'react-router-dom';

const HeaderComp: React.FC = (props: any) => (
  <Router {...props}>

    <header className="header">
      <div className="app__adaptive-container">
        <div className="header__logo">
          <Link to="/">Logo</Link>
        </div>
        <div className="header__middle">middle</div>
        <div className="header__user">
          <Button
            minimal={true}
            rightIcon="log-in"
            onClick={() => props.history.push('login')}
          >Log in</Button>
        </div>
      </div>
    </header>

  </Router>
);

export const Header = withRouter(HeaderComp);
